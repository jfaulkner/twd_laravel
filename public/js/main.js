$(document).ready(function() {
    
    var headerHeight = $('#main-nav').outerHeight(),
        footerHeight =  $('#footer').outerHeight(),
        mainHeight = $('.container').outerHeight(),
        windowHeight = $(window).height();    
        
    if( mainHeight < (windowHeight - headerHeight - footerHeight)) {
        $('.container').css({
            'minHeight' : (windowHeight - headerHeight - footerHeight) + 'px'
        });
    }   
    
    $('.has-sub').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('tap');
    });   
    
    // Used to ensure that columns are the same height
    // within a row
    var heightArray = $(".row > div").map( function(){
        return  $(this).height();
    }).get();
    var maxHeight = Math.max.apply( Math, heightArray) + 10;
    $(".row > div").height(maxHeight);
     
});