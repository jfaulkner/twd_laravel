<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function showIndex() {
        $title = 'Index';
        
        // Get all posts from the database
        $posts = Post::all();
        
        // Return the corresponding view with the page
        // title and the post information from the database
        return View::make('home.index')
                        ->with('title', $title)
                        ->with('posts', $posts);
    }

    public function showComics() {
        $title = 'Comics';
        
        // Get all comics from the database
        $comics = Comic::all();
        
        // Return the corresponding view with the page
        // title and the comic information from the database
        return View::make('home.comics')
                        ->with('title', $title)
                        ->with('comics', $comics);
    }

    public function showGames() {
        $title = 'Games';
        return View::make('home.games')
                        ->with('title', $title);
    }

    public function showTvCast() {
        $title         = 'TV Cast';
        
        // Get all information for characters from the
        // television show
        $tv_characters = Character::where('type', '=', 'television')->get();
        
        // Return the corresponding view with the page
        // title and the character information from the database
        return View::make('home.tvcast')
                        ->with('title', $title)
                        ->with('characters', $tv_characters);
    }

}
