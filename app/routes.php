<?php

Route::get('/', 'HomeController@showIndex');
Route::get('comics', 'HomeController@showComics');
Route::get('games', 'HomeController@showGames');
Route::get('tvcast', 'HomeController@showTvCast');
