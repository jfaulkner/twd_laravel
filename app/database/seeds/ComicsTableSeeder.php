<?php

class ComicsTableSeeder extends Seeder {
    public function run() {
        // Delete table records
        DB::table("comics")->delete();
        
        // insert records
        DB::table("comics")->insert(array(
            array("title" => "Volume 1: Days Gone By", "image" => "img/comics/volume1.jpg", "issue_range" => "1-6", "description" => "Volume 1 introduces Rick Grimes, the main protagonist, as he emerges from his coma in an abandoned hospital. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
            array("title" => "Volume 2: Miles Behind Us", "image" => "img/comics/volume2.jpg", "issue_range" => "7-12", "description" => "Volume 2 lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
            array("title" => "Volume 3: Safety Behind Bars", "image" => "img/comics/volume3.jpg", "issue_range" => "13-18", "description" => "Volume 3 lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.mg/comics/volume3.jpg"),
            array("title" => "Volume 4: The Heart's Desire", "image" => "img/comics/volume4.jpg", "issue_range" => "19-24", "description" => "Volume 4 lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
            array("title" => "Volume 5: The Best Defense", "image" => "img/comics/volume5.jpg", "issue_range" => "25-30", "description" => "Volume 5 lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),            
        ));
    }
}