<?php

class PostsTableSeeder extends Seeder {
    
    public function run() {
        // Delete table records
        DB::table('posts')->delete();
        
        // insert records
        DB::table('posts')->insert(array(
            array('title' => 'Welcome', 'post_img' => 'img/welcome.jpg', 'created_by' => 'Jerome Faulkner', 'description' => 'Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.
                
                Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.', 'content' => ''),
            array('title' => 'Dead Yourself', 'post_img' => 'img/deadyourself.jpg', 'created_by' => 'Jerome Faulkner', 'description' => 'Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.Lorem ipsum dolor sit amet, ius erat brute aliquip ei, at pro erroribus consetetur. Ipsum viris dolore cu mel, te vel aeque dicam percipitur. Vix ea dolor iisque temporibus. Est an nostro integre hendrerit, reque ocurreret ad nec.', 'content' => '')
        ));
    }
}

