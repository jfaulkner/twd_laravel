@extends('layouts.master')

@section('content')

	<div class="article">
        <h3 class="heading">Episode 1: "A New Day"</h3>                             
        <p>
            <img class="articleImage" src="img/games/episode1.jpg" alt="Episode 1 Screenshot"/>
            Episode 1 introduces the player to Lee Everett, the main protagonist the player will be controlling. Lee awakes to a world where the dead are roaming the earth and stumbles upon a little girl named Clementine who he takes under his wing.</p>
        <h5 class="heading"><a class="articleLink" href="http://www.telltalegames.com/walkingdead/episodes/#episode2">Learn More</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Reviews</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Purchase</a></h5>
    </div>

    <div class="article">
        <h3 class="heading"><a href="#">Episode 2: "Starved For Help"</a></h3>                             
        <p>
            <img class="articleImage" src="img/games/episode2.jpg" alt="Episode 2 Screenshot"/>
            Episode 2 challenges the player to make choices about what they would do in order to survive as the threat of, not only zombies, but also other groups begins to grow.</p>
        <h5 class="heading"><a class="articleLink" href="http://www.telltalegames.com/walkingdead/episodes/#episode2">Learn More</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Reviews</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Purchase</a></h5>
    </div>

    <div class="article">
        <h3 class="heading">Episode 3: "Long Road Ahead"</h3>                            
        <p>
            <img class="articleImage" src="img/games/episode3.jpg" alt="Episode 3 Screenshot"/>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        <h5 class="heading"><a class="articleLink" href="http://www.telltalegames.com/walkingdead/episodes/#episode2">Learn More</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Reviews</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Purchase</a></h5>
    </div>

    <div class="article">
        <h3 class="heading">Episode 4: "Around Every Corner"</h3>                            
        <p>
            <img class="articleImage" src="img/games/episode4.jpg" alt="Episode 4 Screenshot"/>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        <h5 class="heading"><a class="articleLink" href="http://www.telltalegames.com/walkingdead/episodes/#episode2">Learn More</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Reviews</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Purchase</a></h5>
    </div>
    
    <div class="article">
        <h3 class="heading">Episode 5: "No Time Left"</h3>                             
        <p>
            <img class="articleImage" src="img/games/episode5.jpg" alt="Episode 5 Screenshot"/>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        <h5 class="heading"><a class="articleLink" href="http://www.telltalegames.com/walkingdead/episodes/#episode2">Learn More</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Reviews</a></h5>
        <h5 class="heading"><a class="articleLink" href="#">Purchase</a></h5>
    </div>

@stop