@extends('layouts.master')

@section('content')

<!-- Display the information for each post -->
@foreach ($posts as $post)

<div class="article">
    <div class="post-creator">
        Posted by {{ $post->created_by }}
    </div>
    <img src="{{ $post->post_img }}">
    <div class="post-content">
        <h1><a href="#">{{ $post->title }}</a></h1>  
        <hr>
        <p class="post-info">{{ nl2br($post->description) }}</p>
        <a href="#" class="read-more">Read More</a>
    </div>
</div>

@endforeach

@stop