@extends('layouts.master')

@section('content')
    <?php $count = 0; ?>
	<div class="row">
        <!-- Display the information for each character -->
        @foreach ($characters as $character)
        
        <?php $count++; ?>
        <div class="five columns card">
            <h3 class="heading">{{ $character->name }}</h3>
            <p><img src="{{ $character->image }}" alt="{{ $character->name }} picture"/>{{ $character->description }}</p>
            <div class="heading"><a class="articleLink" href="{{ $character->info_link }}">Learn More</a></div>
        </div>

            <!-- Close and start a new row every 3 characters -->
            @if ($character->id % 3 == 0)
                </div>
                <div class="row">
            @endif
            
       @endforeach
                </div>

@stop