@extends('layouts.master')

@section('content')
    <?php $count = 0; ?>
	<div class="row">
        <!-- Display information for each comic -->
        @foreach ($comics as $comic)
        
        <?php $count++; ?>
        <div class="five columns card">
            <h3 class="heading">{{ $comic->title }}</h3>
            <p><img src="{{ $comic->image }}" alt="{{ $comic->title }} picture"/><strong><em>Issues {{ $comic->issue_range }}.</em></strong> {{ $comic->description }}</p>
            <div class="heading"><a class="articleLink" href="#">Learn More</a></div>
        <div class="heading"><a class="articleLink" href="#">Purchase</a></div>
        </div>
        
        <!-- Close and start a new row every 3 comics -->
            @if ($count % 3 == 0)
                </div>
                <div class="row">
            @endif
       @endforeach
                </div>

@stop                            