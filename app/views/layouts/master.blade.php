<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $title }} - TWD Fan Site</title>
        {{ HTML::style('css/normalize.css') }}
        {{ HTML::style('css/main.css') }} 
        {{ HTML::style('css/grid.css') }} 

        {{ HTML::script('js/jquery-1.11.0.min.js') }}
        {{ HTML::script('js/main.js') }}        
        {{ HTML::script('js/prefixfree.min.js') }}
    </head>
    <body>
        <!-- Nav Bar -->
        <nav id="main-nav">
            <ul class="menu">
                <li>{{ HTML::link('/', 'HOME') }}</li>
                <li>{{ HTML::link('#', 'AUTHORS') }}</li>
                <li>{{ HTML::link('tvcast', 'TV CAST') }}</li>
                <li>{{ HTML::link('#', 'COMIC CAST') }}</li>
                <li>{{ HTML::link('comics', 'COMICS', array('rel' => 'external')) }}</li>
                <li>{{ HTML::link('#', 'GAME: Season 1') }}</li>
                <li>{{ HTML::link('#', 'GAME: Season 2')}}</li>
            </ul>		
        </nav>
<!--        <nav id="main-nav">
            <ul class="menu">
                <li>{{ HTML::link('/', 'HOME') }}</li>
                <li>{{ HTML::link('#', 'AUTHORS') }}</li>
                <li class="has-sub">{{ HTML::link('#', 'CAST') }}
                    <ul class="sub-menu">
                        <li><a href="http://localhost/laravel/TWD_Laravel/public/tvcast">TV CAST</a></li>
                        <li>{{ HTML::link('#', 'COMIC CAST') }}</li>
                    </ul>
                </li>
                <li>{{ HTML::link('comics', 'COMICS', array('rel' => 'external')) }}</li>
                <li class="has-sub">{{ HTML::link('games', 'GAMES') }}
                    <ul class="sub-menu">
                        <li>{{ HTML::link('#', 'Season 1') }}</li>
                        <li>{{ HTML::link('#', 'Season 2')}}</li>
                    </ul>
                </li>
            </ul>		
        </nav>-->

        

        <div class="container">                     
            <!-- Content -->
            <div class="content">
                <div id="main" class="twelve columns alpha">
                    @yield('content')

                </div>	
            </div>
            
            <aside id="sideLinks" class="four column omega">
            <h3 class="ribbon">Useful Links</h3>
            <ul>
                <li><a href="http://www.amctv.com/shows/the-walking-dead">The Walking Dead on AMC</a></li>
                <li><a href="http://www.amctv.com/shows/the-walking-dead/episodes">List of Episodes</a></li>
                <li><a href="http://www.amctv.com/shows/talking-dead">The Talking Dead</a></li>
                <li><a href="http://www.telltalegames.com/walkingdead">The Walking Dead Video Game</a></li>
                <li><a href="http://walkingdead.wikia.com/wiki/The_Walking_Dead_Wiki">The Walking Dead wiki</a></li>
            </ul>
        </aside>
        </div>	
        <!-- Footer -->
        <div id="footer">
            <p>&copy; Copyright 2013 Contact: <a href="mailto:faulknerjerome@gmail.com">Jerome Faulkner</a></p>
        </div>	 

    </body>
</html>