<?php

namespace App\Services;

use Config, File, Loag;

class Image {
    
    /**
     * Instance of the Imagine package
     * @var Imagine\Gd\Imagine
     */
    protected $imagine;
    
    /**
     * Type of library used by the service
     * @var string
     */
    protected $library;
    
    /**
     * Initialize the image service
     * @return void
     */
    public function __construct() {
        if (!$this->imagine) {
            $this->library = Config::get('image.library', 'gd');
            
            // Now create the instance
            if ($this->library == 'imagick') {
                $this->imagine = new \Imagine\Imagick\Imagine();
            } elseif ($this->library == 'gmagick') {
                $this->imagine = new \Imagine\Gmagick\Imagine();
            } elseif ($this->library == 'gd') {
                $this->imagine = new \Imagine\Gd\Imagine();
            } else {
                $this->imagine = new \Imagine\Gd\Imagine();
            }
        }
    }
    
    /**
     * Resize an image
     * @param string $url
     * @param integer $width
     * @param integer $height
     * @param boolean $crop
     * @return string
     */
    public function resize($url, $width = 100, $height = null, $crop = false, $quality = 90) {
        if ($url) {
            // URL info
            $info = pathinfo($url);
            
            // The size
            if (!$height) {
                $height = $width;
            }
            
            // Quality
            $quality = Config::get('image.quality', $quality);
        }
    }
}
